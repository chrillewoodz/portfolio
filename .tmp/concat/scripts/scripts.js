/*!
 * Bootstrap v3.3.5 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

/*!
 * Generated using the Bootstrap Customizer (http://getbootstrap.com/customize/?id=a0f63acac7b5cec339e9)
 * Config saved to config.json and https://gist.github.com/a0f63acac7b5cec339e9
 */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(t){"use strict";var e=t.fn.jquery.split(" ")[0].split(".");if(e[0]<2&&e[1]<9||1==e[0]&&9==e[1]&&e[2]<1)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher")}(jQuery),+function(t){"use strict";function e(e){var a,s=e.attr("data-target")||(a=e.attr("href"))&&a.replace(/.*(?=#[^\s]+$)/,"");return t(s)}function a(e){return this.each(function(){var a=t(this),i=a.data("bs.collapse"),n=t.extend({},s.DEFAULTS,a.data(),"object"==typeof e&&e);!i&&n.toggle&&/show|hide/.test(e)&&(n.toggle=!1),i||a.data("bs.collapse",i=new s(this,n)),"string"==typeof e&&i[e]()})}var s=function(e,a){this.$element=t(e),this.options=t.extend({},s.DEFAULTS,a),this.$trigger=t('[data-toggle="collapse"][href="#'+e.id+'"],[data-toggle="collapse"][data-target="#'+e.id+'"]'),this.transitioning=null,this.options.parent?this.$parent=this.getParent():this.addAriaAndCollapsedClass(this.$element,this.$trigger),this.options.toggle&&this.toggle()};s.VERSION="3.3.5",s.TRANSITION_DURATION=350,s.DEFAULTS={toggle:!0},s.prototype.dimension=function(){var t=this.$element.hasClass("width");return t?"width":"height"},s.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var e,i=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing");if(!(i&&i.length&&(e=i.data("bs.collapse"),e&&e.transitioning))){var n=t.Event("show.bs.collapse");if(this.$element.trigger(n),!n.isDefaultPrevented()){i&&i.length&&(a.call(i,"hide"),e||i.data("bs.collapse",null));var l=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[l](0).attr("aria-expanded",!0),this.$trigger.removeClass("collapsed").attr("aria-expanded",!0),this.transitioning=1;var r=function(){this.$element.removeClass("collapsing").addClass("collapse in")[l](""),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!t.support.transition)return r.call(this);var o=t.camelCase(["scroll",l].join("-"));this.$element.one("bsTransitionEnd",t.proxy(r,this)).emulateTransitionEnd(s.TRANSITION_DURATION)[l](this.$element[0][o])}}}},s.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var e=t.Event("hide.bs.collapse");if(this.$element.trigger(e),!e.isDefaultPrevented()){var a=this.dimension();this.$element[a](this.$element[a]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",!1),this.$trigger.addClass("collapsed").attr("aria-expanded",!1),this.transitioning=1;var i=function(){this.transitioning=0,this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")};return t.support.transition?void this.$element[a](0).one("bsTransitionEnd",t.proxy(i,this)).emulateTransitionEnd(s.TRANSITION_DURATION):i.call(this)}}},s.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()},s.prototype.getParent=function(){return t(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each(t.proxy(function(a,s){var i=t(s);this.addAriaAndCollapsedClass(e(i),i)},this)).end()},s.prototype.addAriaAndCollapsedClass=function(t,e){var a=t.hasClass("in");t.attr("aria-expanded",a),e.toggleClass("collapsed",!a).attr("aria-expanded",a)};var i=t.fn.collapse;t.fn.collapse=a,t.fn.collapse.Constructor=s,t.fn.collapse.noConflict=function(){return t.fn.collapse=i,this},t(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(s){var i=t(this);i.attr("data-target")||s.preventDefault();var n=e(i),l=n.data("bs.collapse"),r=l?"toggle":i.data();a.call(n,r)})}(jQuery);
'use strict';

var core = angular.module('portfolio', [
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'smoothScroll'
  ]);

core.directive('closeWindow', [function() {
  
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      
      var $toggle = $('#nav-toggle');
      
      scope.close = function() {
        
        if ($toggle.is(':visible')) {
          element.removeClass('in');
          element.attr('aria-expanded', 'false');
          $toggle.removeClass('x');
          $toggle.addClass('collapsed');
          $toggle.attr('aria-expanded', 'false');
        }
      }
    }
  }
}]);
core.directive('fullHeight', [function() {
  
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      
      var winHeight = $(window).height(),
          headerHeight = $('#site-header').height();
      
      element.css({
        height: winHeight - headerHeight
      });
      
      $(window).resize(function() {
        
        winHeight = $(window).height();
        
        element.css({
          height: winHeight - headerHeight
        });
      });
    }
  }
}]);
core.directive('offsetColor', [function() {

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var colorElems = $('.color-offset', element);

      $(window).on('scroll', function() {
        
        var windowY = $(window).scrollTop(),
            closestElem = null,
            closestDist = null;

        colorElems.each(function() {
          console.log('hi');
          var elem = $(this),
              elemDist = Math.abs(elem.offset().top - windowY);
          
          if (closestElem === null || elemDist < closestDist) {
            closestElem = elem;
            closestDist = elemDist;
          }
        });

        if (closestElem !== null) {
          console.log('an element reached the top');
          $('#nav-wrapper').css('background-color', 'blue');
        }
      });
    }
  }
}]);



/*

core.directive('offsetColor', [function() {
  
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      
      var yPos = element.offset().top - 60,
          $window = $(window),
          $navWrapper = $('#nav-wrapper'),
          color1 = '#2d2d2d',
          color2 = '#e1382d';
        
      function rgb2hex(rgb) {
        
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        
        function hex(x) {
          return ("0" + parseInt(x).toString(16)).slice(-2);
        }
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
      }
      
      var isAtTop = false;

      $window.on('scroll', function() {
        var windowY = $(this).scrollTop();
        
          var elementY = element.offset().top;

          if ((elementY - 60) > windowY && isAtTop === false) {
            $navWrapper.css('background-color', color2);
            isAtTop = false;
          }
          else {
            var bg = element.css('background-color');
                bg = rgb2hex(bg);
          
            if (bg === color1) {
              $navWrapper.css('background-color', color2);
            }
            else if (bg === color2) {
              $navWrapper.css('background-color', color1);
            }
            isAtTop = true;
          }
        console.log(isAtTop);
      });
      
      
      $window.scroll(function() {
        
        if ( $(window).scrollTop() >= yPos ) {
          console.log(element);
          var bg = element.css('background-color');
          
          $navWrapper.css('background-color', bg);
        }
      });
    }
  }
}]);*/
core.directive('xAnimation', [function() {
  
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      
      var $el = $('#' + attrs.id);
      
      $el.on('click', function() {

        if ($el.hasClass('x')) {
          $el.removeClass('x');
        }
        else {
          $el.addClass('x');
        }
      });
    }
  }
}]);
core.factory('fetchContent', ['$http', function($http) {
  
  return {
    getMenu: function() {
      return $http.get('content/menu.json');
    },
    getImages: function() {
      return $http.get('content/images.json');
    },
    getHero: function() {
      return $http.get('content/hero.json');
    },
    getSkillset: function() {
      return $http.get('content/skills.json');
    },
    getProjects: function() {
      return $http.get('content/projects.json');
    },
    getAbout: function() {
      return $http.get('content/about.json');
    },
    getContact: function() {
      return $http.get('content/contact.json');
    },
    getEmailInfo: function() {
      
      return $http.get('content/email-info.json');
    },
  }
}]);
core.service('states', [function() {
  
  return {
    
  }
}]);
core.service('validation', ['$q', function($q) {
  
  return {
    validateEmail: function(data) {
      
      var deferred = $q.defer();
    
      for (var prop in data) {
        if (data[prop] === '') {
          deferred.resolve('Alla fält måste vara ifyllda');
        }
      }

      if (data.message.length > 700) {
        deferred.resolve('Ditt meddelande är för långt');
      }
      else {
        deferred.resolve(true);
      }

      deferred.reject('Ett fel uppstod, försök igen');

      return deferred.promise;
    }
  }
}]);
core.service('email', ['$q', 'fetchContent', function($q, fetchContent) {

  return {
    sendEmail: function(info) {

      // Fetch token and recipient info from JSON file
      var getEmailInfo = fetchContent.getEmailInfo();

      return getEmailInfo.then(function(res) {

        // Access the token and recipient from the JSON file we just requested
        var token = res.data.email.key,
            recipient = res.data.email.recipient;

        // Create a new mandrill instance and set the params
        var emailProvider = new mandrill.Mandrill(token),
            params = {
              "message": {
                "from_email": info.sender,
                "to":[
                  { "email": recipient }
                ],
                "subject": info.company,
                "text": info.message
              }
            };

        // Return a deferred promise containing our status message
        var deferred = $q.defer();

        emailProvider.messages.send(params, function(res) {
          deferred.resolve('Ditt meddelande har skickats');

        }, function(err) {
          deferred.reject('Ett fel uppstod, försök igen');
        })

        return deferred.promise;
      });
    }
  }
}]);
core.controller('MainCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getImages = fetchContent.getImages();
  
  getImages.then(function(res) {
    
    $scope.images = res.data.images;
  });
}]);
core.controller('NavCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getNav = fetchContent.getMenu();
  
  getNav.then(function(res) {
    
    $scope.menu = res.data.menu;
  });
}]);
core.controller('HeroCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getHeroInfo = fetchContent.getHero();
  
  getHeroInfo.then(function(res) {
    
    $scope.heroInfo = res.data.hero;
  });
}]);
core.controller('SkillsCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getSkillset = fetchContent.getSkillset();
  
  getSkillset.then(function(res) {
    
    $scope.skillset = res.data.skills;
  });
}]);
core.controller('ProjectsCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getProjects = fetchContent.getProjects();
  
  getProjects.then(function(res) {
    
    $scope.projects = res.data.projects;
  });
}]);
core.controller('AboutCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getAbout = fetchContent.getAbout();
  
  getAbout.then(function(res) {
    
    $scope.about = res.data.about;
  });
}]);
core.controller('ContactCtrl', ['$scope', 'fetchContent', 'validation', 'email', function($scope, fetchContent, validation, email) {
  
  var getContact = fetchContent.getContact();
  
  getContact.then(function(res) {
    
    $scope.contact = res.data.contact;
  });
  
  $scope.data = {
    name: '',
    company: '',
    sender: '',
    message: ''
  }
  
  $scope.send = function(data) {

    $scope.error = '';
    $scope.status = '';
    
    var validate = validation.validateEmail(data);

    validate.then(function(res) {

      if (res !== true) {
        $scope.error = res;
      }
      else {
        email.sendEmail(data).then(function(res){
          $scope.status = res;
        }, function(err){
          $scope.error = err;
        });
      }
    });
  }
}]);
'use strict';

core.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: '/views/home.html'
    })
    .otherwise({redirectTo: '/'});
}]);