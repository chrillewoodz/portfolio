'use strict';

var core = angular.module('portfolio', [
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'smoothScroll'
  ]);
