core.controller('AboutCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getAbout = fetchContent.getAbout();
  
  getAbout.then(function(res) {
    
    $scope.about = res.data.about;
  });
}]);