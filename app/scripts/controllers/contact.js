core.controller('ContactCtrl', ['$scope', 'fetchContent', 'validation', 'email', function($scope, fetchContent, validation, email) {
  
  var getContact = fetchContent.getContact();
  
  getContact.then(function(res) {
    
    $scope.contact = res.data.contact;
  });
  
  $scope.data = {
    name: '',
    company: '',
    sender: '',
    message: ''
  }
  
  $scope.send = function(data) {

    $scope.error = '';
    $scope.status = '';
    
    var validate = validation.validateEmail(data);

    validate.then(function(res) {

      if (res !== true) {
        $scope.error = res;
      }
      else {
        email.sendEmail(data).then(function(res){
          $scope.status = res;
        }, function(err){
          $scope.error = err;
        });
      }
    });
  }
}]);