core.controller('HeroCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getHeroInfo = fetchContent.getHero();
  
  getHeroInfo.then(function(res) {
    
    $scope.heroInfo = res.data.hero;
  });
}]);