core.controller('ProjectsCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getProjects = fetchContent.getProjects();
  
  getProjects.then(function(res) {
    
    $scope.projects = res.data.projects;
  });
}]);