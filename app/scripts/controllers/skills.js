core.controller('SkillsCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getSkillset = fetchContent.getSkillset();
  
  getSkillset.then(function(res) {
    
    $scope.skillset = res.data.skills;
  });
}]);