core.controller('NavCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getNav = fetchContent.getMenu();
  
  getNav.then(function(res) {
    
    $scope.menu = res.data.menu;
  });
}]);