core.controller('MainCtrl', ['$scope', 'fetchContent', function($scope, fetchContent) {
  
  var getImages = fetchContent.getImages();
  
  getImages.then(function(res) {
    
    $scope.images = res.data.images;
  });
}]);