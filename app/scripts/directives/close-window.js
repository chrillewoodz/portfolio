core.directive('closeWindow', [function() {
  
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      
      var $toggle = $('#nav-toggle');
      
      scope.close = function() {
        
        if ($toggle.is(':visible')) {
          element.removeClass('in');
          element.attr('aria-expanded', 'false');
          $toggle.removeClass('x');
          $toggle.addClass('collapsed');
          $toggle.attr('aria-expanded', 'false');
        }
      }
    }
  }
}]);