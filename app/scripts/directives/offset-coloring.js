core.directive('offsetColor', [function() {

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var colorElems = $('.color-offset', element);

      $(window).on('scroll', function() {
        
        var windowY = $(window).scrollTop(),
            closestElem = null,
            closestDist = null;

        colorElems.each(function() {
          console.log('hi');
          var elem = $(this),
              elemDist = Math.abs(elem.offset().top - windowY);
          
          if (closestElem === null || elemDist < closestDist) {
            closestElem = elem;
            closestDist = elemDist;
          }
        });

        if (closestElem !== null) {
          console.log('an element reached the top');
          $('#nav-wrapper').css('background-color', 'blue');
        }
      });
    }
  }
}]);



/*

core.directive('offsetColor', [function() {
  
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      
      var yPos = element.offset().top - 60,
          $window = $(window),
          $navWrapper = $('#nav-wrapper'),
          color1 = '#2d2d2d',
          color2 = '#e1382d';
        
      function rgb2hex(rgb) {
        
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        
        function hex(x) {
          return ("0" + parseInt(x).toString(16)).slice(-2);
        }
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
      }
      
      var isAtTop = false;

      $window.on('scroll', function() {
        var windowY = $(this).scrollTop();
        
          var elementY = element.offset().top;

          if ((elementY - 60) > windowY && isAtTop === false) {
            $navWrapper.css('background-color', color2);
            isAtTop = false;
          }
          else {
            var bg = element.css('background-color');
                bg = rgb2hex(bg);
          
            if (bg === color1) {
              $navWrapper.css('background-color', color2);
            }
            else if (bg === color2) {
              $navWrapper.css('background-color', color1);
            }
            isAtTop = true;
          }
        console.log(isAtTop);
      });
      
      
      $window.scroll(function() {
        
        if ( $(window).scrollTop() >= yPos ) {
          console.log(element);
          var bg = element.css('background-color');
          
          $navWrapper.css('background-color', bg);
        }
      });
    }
  }
}]);*/