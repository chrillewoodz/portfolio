core.directive('fullHeight', [function() {
  
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      
      var winHeight = $(window).height(),
          headerHeight = $('#site-header').height();
      
      element.css({
        height: winHeight - headerHeight
      });
      
      $(window).resize(function() {
        
        winHeight = $(window).height();
        
        element.css({
          height: winHeight - headerHeight
        });
      });
    }
  }
}]);