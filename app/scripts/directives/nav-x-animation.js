core.directive('xAnimation', [function() {
  
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      
      var $el = $('#' + attrs.id);
      
      $el.on('click', function() {

        if ($el.hasClass('x')) {
          $el.removeClass('x');
        }
        else {
          $el.addClass('x');
        }
      });
    }
  }
}]);