core.service('email', ['$q', 'fetchContent', function($q, fetchContent) {

  return {
    sendEmail: function(info) {

      // Fetch token and recipient info from JSON file
      var getEmailInfo = fetchContent.getEmailInfo();

      return getEmailInfo.then(function(res) {

        // Access the token and recipient from the JSON file we just requested
        var token = res.data.email.key,
            recipient = res.data.email.recipient;

        // Create a new mandrill instance and set the params
        var emailProvider = new mandrill.Mandrill(token),
            params = {
              "message": {
                "from_email": info.sender,
                "to":[
                  { "email": recipient }
                ],
                "subject": info.company,
                "text": info.message
              }
            };

        // Return a deferred promise containing our status message
        var deferred = $q.defer();

        emailProvider.messages.send(params, function(res) {
          deferred.resolve('Ditt meddelande har skickats');

        }, function(err) {
          deferred.reject('Ett fel uppstod, försök igen');
        })

        return deferred.promise;
      });
    }
  }
}]);