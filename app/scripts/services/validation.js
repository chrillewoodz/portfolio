core.service('validation', ['$q', function($q) {
  
  return {
    validateEmail: function(data) {
      
      var deferred = $q.defer();
    
      for (var prop in data) {
        if (data[prop] === '') {
          deferred.resolve('Alla fält måste vara ifyllda');
        }
      }

      if (data.message.length > 700) {
        deferred.resolve('Ditt meddelande är för långt');
      }
      else {
        deferred.resolve(true);
      }

      deferred.reject('Ett fel uppstod, försök igen');

      return deferred.promise;
    }
  }
}]);