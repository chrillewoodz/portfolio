core.factory('fetchContent', ['$http', function($http) {
  
  return {
    getMenu: function() {
      return $http.get('content/menu.json');
    },
    getImages: function() {
      return $http.get('content/images.json');
    },
    getHero: function() {
      return $http.get('content/hero.json');
    },
    getSkillset: function() {
      return $http.get('content/skills.json');
    },
    getProjects: function() {
      return $http.get('content/projects.json');
    },
    getAbout: function() {
      return $http.get('content/about.json');
    },
    getContact: function() {
      return $http.get('content/contact.json');
    },
    getEmailInfo: function() {
      
      return $http.get('content/email-info.json');
    },
  }
}]);